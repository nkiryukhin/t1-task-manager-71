package ru.t1.nkiryukhin.tm.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.nkiryukhin.tm.api.endpoint.*;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;

@Configuration
@ComponentScan("ru.t1.nkiryukhin.tm")
public class ClientConfiguration {

    @Bean
    public ISystemEndpoint systemEndpoint(IPropertyService propertyService)
    {
        return ISystemEndpoint.newInstance(propertyService);
    }

    @Bean
    public IProjectEndpoint projectEndpoint(IPropertyService propertyService)
    {
        return IProjectEndpoint.newInstance(propertyService);
    }

    @Bean
    public ITaskEndpoint taskEndpoint(IPropertyService propertyService)
    {
        return ITaskEndpoint.newInstance(propertyService);
    }

    @Bean
    public IAuthEndpoint authEndpoint(IPropertyService propertyService)
    {
        return IAuthEndpoint.newInstance(propertyService);
    }

    @Bean
    public IAdminEndpoint adminEndpoint(IPropertyService propertyService)
    {
        return IAdminEndpoint.newInstance(propertyService);
    }

    @Bean
    public IUserEndpoint userEndpoint(IPropertyService propertyService)
    {
        return IUserEndpoint.newInstance(propertyService);
    }

}
