package ru.t1.nkiryukhin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.enumerated.CustomSorting;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private CustomSorting sort;

    public TaskListRequest(@Nullable final String token) {
        super(token);
    }

}
