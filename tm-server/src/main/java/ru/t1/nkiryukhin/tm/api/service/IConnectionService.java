package ru.t1.nkiryukhin.tm.api.service;

import liquibase.Liquibase;
import liquibase.exception.DatabaseException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.sql.SQLException;

public interface IConnectionService {

    @NotNull
    Liquibase getLiquibase() throws IOException, SQLException, DatabaseException;

}