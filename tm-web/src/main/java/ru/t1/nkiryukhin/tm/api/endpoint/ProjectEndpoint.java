package ru.t1.nkiryukhin.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.nkiryukhin.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.nio.file.AccessDeniedException;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface ProjectEndpoint {

    @WebMethod
    @DeleteMapping("/clear")
    void clear() throws AccessDeniedException;

    @WebMethod
    @GetMapping("/count")
    long count() throws Exception;

    @WebMethod
    @DeleteMapping("/deleteAll")
    void deleteList(
            @WebParam(name = "projects", partName = "projects")
            @RequestBody List<ProjectDTO> projects
    ) throws AccessDeniedException;

    @WebMethod
    @DeleteMapping("/delete")
    void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDTO project
    ) throws AccessDeniedException;

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) throws AccessDeniedException;

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) throws AccessDeniedException;

    @WebMethod
    @GetMapping("/findAll")
    Collection<ProjectDTO> findAll() throws AccessDeniedException;

    @WebMethod
    @GetMapping("/findById/{id}")
    ProjectDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) throws AccessDeniedException;

    @WebMethod
    @PutMapping("/save")
    void save(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDTO project
    ) throws AccessDeniedException;

}