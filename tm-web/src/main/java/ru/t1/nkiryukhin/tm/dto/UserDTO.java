package ru.t1.nkiryukhin.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "web_user")
public final class UserDTO {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    private String login;

    @Column(name = "password_hash")
    private String passwordHash;

    public UserDTO(String login) {
        this.login = login;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return id.equals(userDTO.id) && login.equals(userDTO.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }

}
