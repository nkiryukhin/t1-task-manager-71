package ru.t1.nkiryukhin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.nkiryukhin.tm.dto.ProjectDTO;

import java.util.Collection;
import java.util.Optional;


@Repository
public interface ProjectDTORepository extends JpaRepository<ProjectDTO, String> {

    long countByUserId(@NotNull final String userId);

    @Transactional
    void deleteAllByUserId(@NotNull final String userId);

    @Transactional
    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    Collection<ProjectDTO> findAllByUserId(@NotNull final String userId);

    @NotNull
    Optional<ProjectDTO> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}