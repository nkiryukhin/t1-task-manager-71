package ru.t1.nkiryukhin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.nkiryukhin.tm.model.Project;
import ru.t1.nkiryukhin.tm.model.User;

import java.util.Collection;
import java.util.Optional;


@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    long countByUserId(@NotNull final String userId);

    @Transactional
    void deleteAllByUserId(@NotNull final String userId);

    @Transactional
    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    Collection<Project> findAllByUserId(@NotNull final String userId);

    @NotNull
    Optional<Project> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}