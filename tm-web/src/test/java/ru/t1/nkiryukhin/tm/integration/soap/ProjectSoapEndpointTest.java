package ru.t1.nkiryukhin.tm.integration.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.data.util.CastUtils;
import org.springframework.http.HttpHeaders;
import ru.t1.nkiryukhin.tm.api.endpoint.AuthEndpoint;
import ru.t1.nkiryukhin.tm.api.endpoint.ProjectEndpoint;
import ru.t1.nkiryukhin.tm.client.soap.AuthSoapEndpointClient;
import ru.t1.nkiryukhin.tm.client.soap.ProjectSoapEndpointClient;
import ru.t1.nkiryukhin.tm.dto.ProjectDTO;
import ru.t1.nkiryukhin.tm.marker.IntegrationCategory;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.util.*;

@Category(IntegrationCategory.class)
public class ProjectSoapEndpointTest {

    @NotNull
    private static AuthEndpoint AUTH_ENDPOINT;

    @NotNull
    private static ProjectEndpoint PROJECT_ENDPOINT;

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1");

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Test Project 2");

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO("Test Project 3");

    @Nullable
    private static String USER_ID;

    @BeforeClass
    public static void beforeClass() throws MalformedURLException {
        AUTH_ENDPOINT = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(AUTH_ENDPOINT.login("test", "test").getSuccess());
        PROJECT_ENDPOINT = ProjectSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) AUTH_ENDPOINT;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) PROJECT_ENDPOINT;
        @NotNull Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        USER_ID = AUTH_ENDPOINT.profile().getId();
    }

    @AfterClass
    public static void logout() {
        AUTH_ENDPOINT.logout();
    }

    @Before
    public void before() throws Exception {
        project1.setUserId(USER_ID);
        project2.setUserId(USER_ID);
        PROJECT_ENDPOINT.save(project1);
        PROJECT_ENDPOINT.save(project2);
    }

    @After
    public void after() throws Exception {
        PROJECT_ENDPOINT.clear();
    }

    @Test
    public void countTest() throws Exception {
        Assert.assertEquals(2, PROJECT_ENDPOINT.count());
    }

    @Test
    public void clearTest() throws Exception {
        PROJECT_ENDPOINT.clear();
        Assert.assertNull(PROJECT_ENDPOINT.findById(project1.getId()));
        Assert.assertNull(PROJECT_ENDPOINT.findById(project2.getId()));
    }

    @Test
    public void deleteTest() throws Exception {
        Assert.assertNotNull(PROJECT_ENDPOINT.findById(project1.getId()));
        PROJECT_ENDPOINT.delete(project1);
        Assert.assertNull(PROJECT_ENDPOINT.findById(project1.getId()));
    }

    @Test
    public void deleteByIdTest() throws Exception {
        Assert.assertNotNull(PROJECT_ENDPOINT.findById(project1.getId()));
        PROJECT_ENDPOINT.deleteById(project1.getId());
        Assert.assertNull(PROJECT_ENDPOINT.findById(project1.getId()));
    }

    @Test
    public void deleteListTest() throws Exception {
        List<ProjectDTO> projects = (List<ProjectDTO>) PROJECT_ENDPOINT.findAll();
        Assert.assertTrue(projects.contains(project1));
        Assert.assertTrue(projects.contains(project2));
        PROJECT_ENDPOINT.deleteList(projects);
        projects = (List<ProjectDTO>) PROJECT_ENDPOINT.findAll();
        Assert.assertNull(projects);
    }

    @Test
    public void findAll() throws Exception {
        Collection<ProjectDTO> projects = PROJECT_ENDPOINT.findAll();
        Assert.assertTrue(projects.contains(project1));
        Assert.assertTrue(projects.contains(project2));
    }

    @Test
    public void findByIdTest() throws Exception {
        Assert.assertEquals(PROJECT_ENDPOINT.findById(project1.getId()), project1);
    }

    @Test
    public void saveTest() throws Exception {
        Assert.assertNull(PROJECT_ENDPOINT.findById(project3.getId()));
        PROJECT_ENDPOINT.save(project3);
        Assert.assertNotNull(PROJECT_ENDPOINT.findById(project3.getId()));
    }

}