package ru.t1.nkiryukhin.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.nkiryukhin.tm.config.*;
import ru.t1.nkiryukhin.tm.dto.ProjectDTO;
import ru.t1.nkiryukhin.tm.repository.ProjectDTORepository;
import ru.t1.nkiryukhin.tm.util.UserUtil;

import java.util.Collection;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DatabaseConfiguration.class, SecurityWebApplicationInitializer.class, ServiceAuthenticationEntryPoint.class, ApplicationConfiguration.class})
public class ProjectControllerTest {

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Unit Project 1");

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Test Unit Project 2");

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private ProjectDTORepository projectRepository;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @Nullable
    private String userId;

    @Before
    public void before() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        project2.setUserId(userId);
        projectRepository.save(project1);
        projectRepository.save(project2);
    }

    @After
    @SneakyThrows
    public void after() {
        projectRepository.deleteAllByUserId(userId);
    }

    @Test
    @SneakyThrows
    public void createTest() {
        @NotNull final String url = "/project/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @NotNull final Collection<ProjectDTO> projects = projectRepository.findAllByUserId(userId);
        Assert.assertNotNull(projects);
        Assert.assertEquals(3, projects.size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final String url = "/project/delete/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @Nullable final ProjectDTO project = projectRepository.findByUserIdAndId(userId, project1.getId()).orElse(null);
        Assert.assertNull(project);
    }

    @Test
    @SneakyThrows
    public void editTest() {
        @NotNull final String url = "/project/edit/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        @NotNull final String url = "/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

}