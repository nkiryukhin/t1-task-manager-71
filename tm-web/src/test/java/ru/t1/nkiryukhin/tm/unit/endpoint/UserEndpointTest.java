package ru.t1.nkiryukhin.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.nkiryukhin.tm.config.*;
import ru.t1.nkiryukhin.tm.dto.UserDTO;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.repository.UserDTORepository;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DatabaseConfiguration.class, SecurityWebApplicationInitializer.class, ServiceAuthenticationEntryPoint.class, ApplicationConfiguration.class})
public class UserEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private static final String USERS_URL = "http://localhost:8080/api/users/";

    @NotNull
    @Autowired
    private UserDTORepository userRepository;

    private long initialUserCount = 0;

    @NotNull
    private final UserDTO user1 = new UserDTO();

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        initialUserCount = userRepository.count();
        user1.setLogin("Test Unit 1");
        add(user1);
    }

    @After
    public void after() {
        try {
            @Nullable final UserDTO userDTO = findById(user1.getId());
            @NotNull final String url = USERS_URL + "deleteById/" + user1.getId();
            mockMvc.perform(MockMvcRequestBuilders.delete(url).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void add(@NotNull final UserDTO user) {
        @NotNull final String url = USERS_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        try {
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);
        mockMvc.perform(MockMvcRequestBuilders.put(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @SneakyThrows
    private UserDTO findById(@NotNull final String id) {
        @NotNull final String url = USERS_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, UserDTO.class);
    }

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        add(user1);
        @Nullable final UserDTO userDTO = findById(user1.getId());
        Assert.assertNotNull(userDTO);
        Assert.assertEquals(userDTO.getId(), user1.getId());
    }

    @Test
    public void countTest() throws Exception {
        @NotNull final String url = USERS_URL + "count";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertEquals(initialUserCount + 1, objectMapper.readValue(json, Integer.class).intValue());
    }

    @Test
    public void deleteTest() throws Exception {
        @NotNull final String url = USERS_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(user1);
        mockMvc.perform(MockMvcRequestBuilders.delete(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(user1.getId()));
    }

    @Test
    public void deleteByIdTest() throws Exception {
        @NotNull final String url = USERS_URL + "deleteById/" + user1.getId();
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(user1.getId()));
    }

    @Test
    public void existsByIdTest() throws Exception {
        @NotNull final String url = USERS_URL + "existsById/" + user1.getId();
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertEquals("true", objectMapper.readValue(json, String.class));
    }

    @Test
    public void findAllTest() throws Exception {
        @NotNull final String url = USERS_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final List<UserDTO> users = Arrays.asList(objectMapper.readValue(json, UserDTO[].class));
        Assert.assertNotNull(users);
        Assert.assertEquals(initialUserCount + 1, users.size());
    }

    @Test
    public void findByIdTest() {
        Assert.assertNotNull(findById(user1.getId()));
    }

    @Test
    public void saveTest() throws Exception {
        user1.setLogin("New Login");
        @NotNull final String url = USERS_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(user1);
        mockMvc.perform(MockMvcRequestBuilders.put(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        @Nullable final UserDTO userDTO = findById(user1.getId());
        Assert.assertEquals(user1.getLogin(), "New Login");
    }

}